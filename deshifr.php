<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$fk_name="key.txt";
$fi_name="shifr.txt";
$fd_name="deshifr.txt";
$ft_name="type_shifr.txt";


/////////////////////////////////////////////////////////////////////////
//Блок имортирования ключа из внешнего файла
/////////////////////////////////////////////////////////////////////////
$fk= fopen($fk_name, 'r');
if(!file_exists($fk_name)){
    echo "Файл ключа не найден!!!Не могу продолжить дальнейщие вычисления!";
}  else {
    $size_key=filesize($fk_name);
    $fk_content=fread($fk, $size_key);
    fclose($fk);
}
/////////////////////////////////////////////////////////////////////////
//Блок чтения входных данных
/////////////////////////////////////////////////////////////////////////
$fi= fopen($fi_name, 'r');
if(!file_exists($fi_name)){
    $flag=1;
    echo "Входной файл не найден!!!Не могу продолжить дальнейщие вычисления!";
}  else {
    $size_input=filesize($fi_name);
    $fi_content=fread($fi, $size_input);
    fclose($fi);
}
/////////////////////////////////////////////////////////////////////////
//Блок чтения метода шифрования
/////////////////////////////////////////////////////////////////////////
$ft= fopen($ft_name, 'r');
if(!file_exists($ft_name)){
    $flag=1;
    echo "Неизвестен метод шифрования!";
}  else {
    $size_type=filesize($ft_name);
    $ft_content=fread($ft, $size_input);
    fclose($ft);
}



$key=$fk_content;
$input=$fi_content;
$type=$ft_content;

if($flag!=1){
    
if($type=="ECB"){
    $encrypted_data = mcrypt_ecb (MCRYPT_BLOWFISH, $key, $input, MCRYPT_DECRYPT);
    $encrypted_data=trim($encrypted_data);

    $fd = fopen($fd_name, 'w');
if(!file_exists($fd_name)){
    file_put_contents($fd_name, $encrypted_data);
}  else {
    flock($fd, LOCK_EX);
    ftruncate($fd, 0);
    fwrite($fd, $encrypted_data);
    flock($fd, LOCK_UN);
    fclose($fd);
}
    
    echo $encrypted_data."<br>";

}elseif ($type=="CBC") {
   $encrypted_data = mcrypt_cbc (MCRYPT_BLOWFISH, $key, $input, MCRYPT_DECRYPT); 
   $encrypted_data=trim($encrypted_data);
   
   $fd= fopen($fd_name, 'w');
if(!file_exists($fd_name)){
    file_put_contents($fd_name, $encrypted_data);
}  else {
    flock($fd, LOCK_EX);
    ftruncate($fd, 0);
    fwrite($fd, $encrypted_data);
    flock($fd, LOCK_UN);
    fclose($fd);
}
   
   
   echo $encrypted_data;
}elseif ($type=="CFB") {   
    $encrypted_data = mcrypt_cfb (MCRYPT_BLOWFISH, $key, $input, MCRYPT_DECRYPT);
    $encrypted_data=trim($encrypted_data);
    
    $fd= fopen($fd_name, 'w');
if(!file_exists($fd_name)){
    file_put_contents($fd_name, $encrypted_data);
}  else {
    flock($fd, LOCK_EX);
    ftruncate($fd, 0);
    fwrite($fd, $encrypted_data);
    flock($fd, LOCK_UN);
    fclose($fd);
}
    
    
    echo $encrypted_data;
}elseif ($type=="OFB") {
    $encrypted_data = mcrypt_ofb (MCRYPT_BLOWFISH, $key, $input, MCRYPT_DECRYPT);
    $encrypted_data=trim($encrypted_data);
    
    
    $fd= fopen($fd_name, 'w');
if(!file_exists($fd_name)){
    file_put_contents($fd_name, $encrypted_data);
}  else {
    flock($fd, LOCK_EX);
    ftruncate($fd, 0);
    fwrite($fd, $encrypted_data);
    flock($fd, LOCK_UN);
    fclose($fd);
}
    
    
    echo $encrypted_data;
}  else {
echo "Выберите метод шифрования!!!";    
}

}

?>
