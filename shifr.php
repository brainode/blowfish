       <?php
    
$type  = $_POST["type"];

$fk_name="key.txt";
$fs_name="shifr.txt";
$fi_name="input.txt";

/////////////////////////////////////////////////////////////////////////
//Блок имортирования ключа из внешнего файла
/////////////////////////////////////////////////////////////////////////
$fk= fopen($fk_name, 'r');
if(!file_exists($fk_name)){
    echo "Файл ключа не найден!!!Не могу продолжить дальнейщие вычисления!";
}  else {
    $size_key=filesize($fk_name);
    $fk_content=fread($fk, $size_key);
    fclose($fk);
}
/////////////////////////////////////////////////////////////////////////
//Блок чтения входных данных
/////////////////////////////////////////////////////////////////////////
$fi= fopen($fi_name, 'r');
if(!file_exists($fi_name)){
    $flag=1;
    echo "Входной файл не найден!!!Не могу продолжить дальнейщие вычисления!";
}  else {
    $size_input=filesize($fi_name);
    $fi_content=fread($fi, $size_input);
    fclose($fi);
}
/////////////////////////////////////////////////////////////////////////
//Блок записи метода шифрования для последующего дешифрования
/////////////////////////////////////////////////////////////////////////
$ft = fopen('type_shifr.txt', 'w');
if(!file_exists('type_shifr.txt')){
    file_put_contents('type_shifr.txt', $type);
}  else {
    flock($ft, LOCK_EX);
    ftruncate($ft, 0);
    fwrite($ft, $type);
    flock($ft, LOCK_UN);
    fclose($ft);
}
/////////////////////////////////////////////////////////////////////////
$key=$fk_content;
$input=$fi_content;

if($flag!=1){
    
if($type=="ECB"){
    $encrypted_data = mcrypt_ecb (MCRYPT_BLOWFISH, $key, $input, MCRYPT_ENCRYPT);

    $fs = fopen($fs_name, 'w');
if(!file_exists($fs_name)){
    file_put_contents($fs_name, $encrypted_data);
}  else {
    flock($fs, LOCK_EX);
    ftruncate($fs, 0);
    fwrite($fs, $encrypted_data);
    flock($fs, LOCK_UN);
    fclose($fs);
}
    
    echo $encrypted_data;
}elseif ($type=="CBC") {
   $encrypted_data = mcrypt_cbc (MCRYPT_BLOWFISH, $key, $input, MCRYPT_ENCRYPT); 
   
   
   $fs= fopen($fs_name, 'w');
if(!file_exists($fs_name)){
    file_put_contents($fs_name, $encrypted_data);
}  else {
    flock($fs, LOCK_EX);
    ftruncate($fs, 0);
    fwrite($fs, $encrypted_data);
    flock($fs, LOCK_UN);
    fclose($fs);
}
   
   
   echo $encrypted_data;
}elseif ($type=="CFB") {   
    $encrypted_data = mcrypt_cfb (MCRYPT_BLOWFISH, $key, $input, MCRYPT_ENCRYPT);
   
    $fs= fopen($fs_name, 'w');
if(!file_exists($fs_name)){
    file_put_contents($fs_name, $encrypted_data);
}  else {
    flock($fs, LOCK_EX);
    ftruncate($fs, 0);
    fwrite($fs, $encrypted_data);
    flock($fs, LOCK_UN);
    fclose($fs);
}
    
    
    echo $encrypted_data;
}elseif ($type=="OFB") {
    $encrypted_data = mcrypt_ofb (MCRYPT_BLOWFISH, $key, $input, MCRYPT_ENCRYPT);
    
    
    $fs= fopen($fs_name, 'w');
if(!file_exists($fs_name)){
    file_put_contents($fs_name, $encrypted_data);
}  else {
    flock($fs, LOCK_EX);
    ftruncate($fs, 0);
    fwrite($fs, $encrypted_data);
    flock($fs, LOCK_UN);
    fclose($fs);
}
    
    
    echo $encrypted_data;
}  else {
echo "Выберите метод шифрования!!!";    
}

}
        ?>
